package org.example;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void add1() {
        assertEquals(3, Calculator.add(1, 2));
    }

    @org.junit.jupiter.api.Test
    void add2() {
        assertEquals(2, Calculator.add(0, 2));
    }
    @org.junit.jupiter.api.Test
    void compare() {
        assertEquals(2, Calculator.compare(1, 2));
        assertEquals(1, Calculator.compare(1, -2));
    }
}